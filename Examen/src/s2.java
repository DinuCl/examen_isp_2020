import javax.swing.*;  
import java.awt.event.*; 

	public class s2 implements ActionListener{  
		
	    JTextField Text1,Text2,TextArea;  
	    JButton Buton1; 
	    
	    s2(){  
	    	
	        JFrame f= new JFrame();  
	        Text1=new JTextField();  
	        Text1.setBounds(50,50,150,20);  
	        
	        Text2=new JTextField();  
	        Text2.setBounds(50,100,150,20);  
	        
	        TextArea=new JTextField();  
	        TextArea.setBounds(50,150,150,20);  
	        TextArea.setEditable(false);   
	        
	        Buton1=new JButton("Suma");  
	        Buton1.setBounds(85,200,100,50);  
	        Buton1.addActionListener(this);  
	        f.add(Text1);f.add(Text2);f.add(TextArea);f.add(Buton1); 
	        
	        f.setSize(300,300);  
	        f.setLayout(null);  
	        f.setVisible(true);  
	    }         
	    public void actionPerformed(ActionEvent e) {  
	        String s1=Text1.getText();  
	        String s2=Text2.getText();
	        
	        int a=Integer.parseInt(s1);  
	        int b=Integer.parseInt(s2);
	        
	        int c=0;
	        
	        if(e.getSource()==Buton1){  
	            c=a+b;  
	        }
	        
	        String rezultat=String.valueOf(c);  
	        TextArea.setText(rezultat);  
	    }  
	    
	public static void main(String[] args) {  
		
	    new s2();  
	}  

}
